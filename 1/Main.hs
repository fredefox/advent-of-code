{-# LANGUAGE TypeApplications, ViewPatterns, BangPatterns #-}

import Data.IntSet (IntSet)
import qualified Data.IntSet as IntSet
import Data.List (foldl')

import Debug.Trace

getInput :: IO [Int]
getInput = fmap go . lines <$> readFile "input.txt"
  where
  go :: String -> Int
  go = read @Int . filter (/= '+')

main = do
  xs <- getInput
  putStrLn $ show $ sum xs
  let sums = scanl (+) 0 $ cycle xs
  putStrLn $ case go sums mempty of
    Just answer -> show answer
    Nothing     -> "No repitition found"
  where
  go :: [Int] -> IntSet -> Maybe Int
  go [] _ = Nothing
  go (x:xs) k =
    if IntSet.member x k
    then pure x
    else go xs $ IntSet.insert x k

  
