{-# LANGUAGE TypeApplications, ViewPatterns, BangPatterns, LambdaCase #-}

import Data.IntSet (IntSet)
import qualified Data.IntSet as IntSet
import Data.List (foldl')
import Data.List (sort, group)
import Control.Monad (guard, join)

import Debug.Trace

getInput :: IO [String]
getInput = lines <$> readFile "input.txt"

main :: IO ()
main = do
  xs <- getInput
  print $ solve xs
  solve2 xs


solve :: [String] -> Int
solve xs = checksum $ foldl step (0, 0) xs
  where
  step :: (Int, Int) -> String -> (Int, Int)
  step (twos , threes) x = case classify x of
    (twos' , threes') -> (f twos' twos , f threes' threes)
  f :: Bool -> Int -> Int
  f True  = succ
  f False = id

-- | Classify answers the two questions: Does the string contain some
-- letter exactly 1) twice or 3) three times.
classify :: String -> (Bool, Bool)
classify x = (2 `elem` lengths, 3 `elem` lengths)
  where
  lengths :: [Int]
  lengths = map length $ group $ sort x

-- Multiply two numbers together
checksum :: (Int, Int) -> Int
checksum (a , b) = a * b

findSim :: [String] -> [(String, String)]
findSim = \case
  []     -> []
  (x:xs) -> join (map go xs) <> findSim xs
     where
     go :: String -> [(String, String)]
     go y = if similar x y then [(x, y)] else []

solve2 :: [String] -> IO ()
solve2 xs = case findSim xs of
  []    -> putStrLn "No matches found"
  ((x, y):_) -> putStrLn $ fixup x y
  where
  fixup :: String -> String -> String
  fixup xs ys = map fst $ filter (\(a, b) -> a == b) $ zip xs ys
 
similar :: String -> String -> Bool
similar xs ys = (<= 1) $ length $ filter not $ zipWith (==) xs ys
